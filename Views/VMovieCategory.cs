﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Views
{
    public class VMovieCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public int? ReleaseYear { get; set; }
        public string PosterUri { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
