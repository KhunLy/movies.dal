USE master
GO

IF EXISTS(SELECT name FROM sys.sysdatabases WHERE name = 'MoviesDB')
BEGIN
	DROP DATABASE [MoviesDB];
END
CREATE DATABASE [MoviesDB];

GO

USE MoviesDB

CREATE TABLE [Category](
	[id] INT NOT NULL IDENTITY,
	[name] VARCHAR(50) NOT NULL,
	[description] VARCHAR(255) NULL,
	CONSTRAINT [PK_Category] PRIMARY KEY([id]),
	CONSTRAINT [UQ_Category_name] UNIQUE([name])
);

CREATE TABLE [Movie](
	[id] INT NOT NULL IDENTITY,
	[title] VARCHAR(100) NOT NULL,
	[synopsis] NVARCHAR(MAX) NULL,
	[release_year] INT NULL,
	[poster_uri] VARCHAR(255) NULL,
	[category_id] INT NULL,
	CONSTRAINT [PK_Movie] PRIMARY KEY([id]),
	CONSTRAINT [FK_Movie_Category]
		FOREIGN KEY ([category_id]) REFERENCES [Category]([id])
		ON DELETE SET NULL
);

CREATE TABLE [Actor](
	[id] INT NOT NULL IDENTITY,
	[last_name] VARCHAR(50) NOT NULL,
	[first_name] VARCHAR(50) NOT NULL,
	[birth_date] DATE NULL,
	[image_uri] VARCHAR(255) NULL,
	CONSTRAINT [PK_Actor] PRIMARY KEY([id]),
);

CREATE TABLE [Cast](
	[movie_id] INT NOT NULL,
	[actor_id] INT NOT NULL,
	[character_name] VARCHAR(255) NOT NULL,
    CONSTRAINT [PK_Cast] PRIMARY KEY([movie_id],[actor_id]),
	CONSTRAINT [FK_Cast_Actor]
		FOREIGN KEY ([actor_id]) REFERENCES [Actor]([id])
		ON DELETE CASCADE,
	CONSTRAINT [FK_Cast_Movie]
		FOREIGN KEY ([movie_id]) REFERENCES [Movie]([id])
		ON DELETE CASCADE,
);

CREATE TABLE [User](
	[id] INT NOT NULL IDENTITY,
	[login] VARCHAR(255) NOT NULL,
	[email] VARCHAR(255) NOT NULL,
	[last_name] VARCHAR(50) NOT NULL,
	[first_name] VARCHAR(50) NOT NULL,
	[encoded_password] VARBINARY(MAX) NOT NULL,
	[salt] VARCHAR(255) NOT NULL,
	[role] VARCHAR(25) NOT NULL DEFAULT 'SIMPLE_USER',
	CONSTRAINT [PK_User] PRIMARY KEY([id]),
	CONSTRAINT [UQ_User_email] UNIQUE([email]),
	CONSTRAINT [UQ_User_login] UNIQUE([login]),
	CONSTRAINT [UQ_User_salt] UNIQUE([salt])
);

GO

CREATE INDEX [IX_Movie_title] ON [Movie]([title]); 
CREATE INDEX [IX_Movie_release_year] ON [Movie]([release_year]);

GO

CREATE VIEW [V_Movie_Category] AS (
	SELECT
		M.*,
		C.[name] AS [category_name]
	FROM [Movie] AS M
	LEFT JOIN [Category] AS C
		ON C.[id] = M.[category_id]
);

GO

CREATE VIEW [V_Casting] AS (
	SELECT
		C.*,
		A.[last_name] AS [actor_last_name],
		A.[first_name] AS [actor_first_name],
		A.[birth_date] AS [actor_birth_date],
		A.[image_uri] AS [actor_image_uri],
		M.[title] AS [movie_title]
	FROM [Cast] AS C
	LEFT JOIN [Movie] AS M
		ON C.[movie_id] = M.[id]
	LEFT JOIN [Actor] AS A
		ON C.[actor_id] = A.[id]
);

GO

INSERT INTO [Category] ([name]) VALUES 
	('Comedie'), ('Animation'), ('Science Fiction');

INSERT INTO [Movie] ([title], [synopsis], [release_year], [category_id]) VALUES 
	('Le monde de Nemo', 'Un petit poisson est perdu', 2003, 2), 
	('Star Wars IV - Un nouvel espoir', 'Luke et Han veulent se taper Leila', 1977, 3), 
	('Star Wars V - L''Empire contre-attaque', 'Luke s''entraine avec un petit homme vert', 1980, 3);

INSERT INTO [Actor]([last_name], [first_name], [birth_date]) VALUES
	('Hamil', 'Mark', CONVERT(DATE, '25/09/1951', 103)),
	('Ford', 'Harisson', CONVERT(DATE, '13/07/1942', 103)),
	('Guiness', 'Alec', CONVERT(DATE, '02/04/1914', 103)),
	('Brooks', 'Albert', CONVERT(DATE, '22/07/1947', 103));

INSERT INTO [Cast]([movie_id], [actor_id], [character_name]) VALUES
	(1,4,'Marin'),
	(2,1,'Luke Skywalker'),
	(2,2,'Han Solo'),
	(2,3,'Obiwan Kenobi'),
	(3,1,'Luke Skywalker'),
	(3,2,'Han Solo');

INSERT INTO [User]([login], [email], [encoded_password], [salt], [last_name], [first_name], [role]) VALUES
	('sa', 'khun.ly@bstorm.be', HASHBYTES('SHA2_512', 'admin' + 'sel1'), 'sel1', 'Admin', 'Admin', 'ADMIN'),
	('khun', 'lykhun@gmail.com', HASHBYTES('SHA2_512', 'khun' + 'sel2'), 'sel2', 'Ly', 'Khun', 'SIMPLE_USER');