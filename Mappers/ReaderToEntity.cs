﻿using Movies.DAL.Entities;
using Movies.DAL.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Mappers
{
    public static class ReaderToEntity
    {
        public static User MapToUser(this IDataReader reader)
        {
            return new User
            {
                Id = (int)reader["id"],
                Login = (string)reader["login"],
                Email = (string)reader["email"],
                LastName = (string)reader["last_name"],
                FirstName = (string)reader["first_name"],
                EncodedPassword = (byte[])reader["encoded_password"],
                Salt = (string)reader["salt"]
            };
        }

        public static Movie MapToMovie(this IDataReader reader)
        {
            return new Movie
            {
                Id = (int)reader["id"],
                CategoryId = reader["category_id"] as int?,
                Title = (string)reader["title"],
                ReleaseYear = reader["release_year"] as int?,
                Synopsis = reader["synopsis"] as string,
                PosterUri = reader["poster_uri"] as string
            };
        }

        public static Actor MapToActor(this IDataReader reader)
        {
            return new Actor
            {
                Id = (int)reader["id"],
                BirthDate = reader["birth_date"] as DateTime?,
                FirstName = (string)reader["first_name"],
                LastName = (string)reader["last_name"],
                ImageUri = reader["image_uri"] as string
            };
        }

        public static Cast MapToCast(this IDataReader reader) 
        {
            return new Cast
            {
                MovieId = (int)reader["movie_id"],
                ActorId = (int)reader["actor_id"],
                CharacterName = (string)reader["character_name"]
            };
        }

        public static Category MapToCategory(this IDataReader reader)
        {
            return new Category
            {
                Id = (int)reader["id"],
                Name = (string)reader["name"],
                Description = reader["description"] as string
            };
        }

        public static VMovieCategory MapToVMovieCategory(this IDataReader reader)
        {
            return new VMovieCategory
            {
                Id = (int)reader["id"],
                CategoryId = reader["category_id"] as int?,
                Title = (string)reader["title"],
                ReleaseYear = reader["release_year"] as int?,
                Synopsis = reader["synopsis"] as string,
                PosterUri = reader["poster_uri"] as string,
                CategoryName = reader["category_name"] as string
            };
        }

        public static VCasting MapToVCasting(this IDataReader reader)
        {
            return new VCasting
            {
                MovieId = (int)reader["movie_id"],
                ActorId = (int)reader["actor_id"],
                CharacterName = (string)reader["character_name"],
                MovieTitle = (string)reader["movie_title"],
                ActorFirstName = (string)reader["actor_first_name"],
                ActorLastName = (string)reader["actor_last_name"],
                ActorBirthDate = reader["actor_birth_date"] as DateTime?,
                ActorImageUri = reader["actor_image_uri"] as string,
            };
        }
    }
}
